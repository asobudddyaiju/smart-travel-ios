// To parse this JSON data, do
//
//     final contactUsResponse = contactUsResponseFromJson(jsonString);

import 'dart:convert';

ContactUsResponse contactUsResponseFromJson(String str) => ContactUsResponse.fromJson(json.decode(str));

String contactUsResponseToJson(ContactUsResponse data) => json.encode(data.toJson());

class ContactUsResponse {
  ContactUsResponse({
    this.id,
    this.name,
    this.email,
    this.contactNumber,
    this.subject,
    this.message,
    this.created,
  });

  final int id;
  final String name;
  final String email;
  final String contactNumber;
  final String subject;
  final String message;
  final DateTime created;

  factory ContactUsResponse.fromJson(Map<String, dynamic> json) => ContactUsResponse(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    contactNumber: json["contact_number"] == null ? null : json["contact_number"],
    subject: json["subject"] == null ? null : json["subject"],
    message: json["message"] == null ? null : json["message"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "contact_number": contactNumber == null ? null : contactNumber,
    "subject": subject == null ? null : subject,
    "message": message == null ? null : message,
    "created": created == null ? null : created.toIso8601String(),
  };
}
