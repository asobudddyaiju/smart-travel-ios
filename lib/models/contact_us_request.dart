// To parse this JSON data, do
//
//     final contactUsRequest = contactUsRequestFromJson(jsonString);

import 'dart:convert';

ContactUsRequest contactUsRequestFromJson(String str) => ContactUsRequest.fromJson(json.decode(str));

String contactUsRequestToJson(ContactUsRequest data) => json.encode(data.toJson());

class ContactUsRequest {
  ContactUsRequest({
    this.apiKey,
    this.name,
    this.email,
    this.contactNumber,
    this.subject,
    this.message,
  });

  final String apiKey;
  final String name;
  final String email;
  final String contactNumber;
  final String subject;
  final String message;

  factory ContactUsRequest.fromJson(Map<String, dynamic> json) => ContactUsRequest(
    apiKey: json["api_key"] == null ? null : json["api_key"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    contactNumber: json["contact_number"] == null ? null : json["contact_number"],
    subject: json["subject"] == null ? null : json["subject"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "api_key": apiKey == null ? null : apiKey,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "contact_number": contactNumber == null ? null : contactNumber,
    "subject": subject == null ? null : subject,
    "message": message == null ? null : message,
  };
}
