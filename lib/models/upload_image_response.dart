// To parse this JSON data, do
//
//     final uploadImageResponse = uploadImageResponseFromJson(jsonString);

import 'dart:convert';

UploadImageResponse uploadImageResponseFromJson(String str) => UploadImageResponse.fromJson(json.decode(str));

String uploadImageResponseToJson(UploadImageResponse data) => json.encode(data.toJson());

class UploadImageResponse {
  UploadImageResponse({
    this.id,
    this.imageFile,
    this.created,
  });

  final int id;
  final String imageFile;
  final DateTime created;

  factory UploadImageResponse.fromJson(Map<String, dynamic> json) => UploadImageResponse(
    id: json["id"] == null ? null : json["id"],
    imageFile: json["image_file"] == null ? null : json["image_file"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "image_file": imageFile == null ? null : imageFile,
    "created": created == null ? null : created.toIso8601String(),
  };
}
