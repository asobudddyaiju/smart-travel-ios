import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/screens/application_placed.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/apply_visa_text_field.dart';
import 'package:smart_travel/widgets/apply_visa_text_field_mobile_no.dart';
import 'package:smart_travel/widgets/calendar_holo.dart';
import 'package:smart_travel/widgets/image_upload.dart';
import 'home_screen.dart';
import 'package:email_validator/email_validator.dart';
import 'package:progress_indicators/progress_indicators.dart';

class ApplyForVisa extends StatefulWidget {
  int id;

  ApplyForVisa({this.id});

  @override
  _ApplyForVisaState createState() => _ApplyForVisaState();
}

class _ApplyForVisaState extends State<ApplyForVisa> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  DateTime entryDate = DateTime.now();
  TextEditingController emailText = TextEditingController();
  TextEditingController countryCodeText = TextEditingController();
  TextEditingController nationalityText = TextEditingController();
  TextEditingController contactText = TextEditingController();
  TextEditingController fullnameText = TextEditingController();
  AppBloc appBloc = AppBloc();
  String url1, url2, url3, url4, url5, applicationId;
  bool loading = false;

  @override
  void initState() {
    appBloc.applyVisaResponse.listen((event) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => ApplicationPlaced(
                applicationId: applicationId,
              )));

      setState(() {
        loading = false;
        applicationId = event.applicationId.toString();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      floatingActionButton: FloatingActionButton(
        backgroundColor: Constants.kitGradients[3],
        onPressed: () async {
          if (fullnameText.text.isNotEmpty &&
              emailText.text.isNotEmpty &&
              nationalityText.text.isNotEmpty &&
              countryCodeText.text.isNotEmpty &&
              contactText.text.isNotEmpty &&
              url1!=null &&
              url2!=null &&
              url3!=null &&
              url4!=null &&
              url5!=null &&
              EmailValidator.validate(emailText.text)==true
          ) {
            setState(() {
              loading = true;
            });
            await appBloc.applyVisaList(
                applyVisaRequest: ApplyVisaRequest(
                    apiKey: "9fef391a-596a-4f04-81d9-e3fa4a5ebfe3",
                    visa: widget.id.toString(),
                    name: fullnameText.text,
                    email: emailText.text,
                    nationality: nationalityText.text,
                    countryCode: countryCodeText.text,
                    dateOfEntry: entryDate.year.toString() +
                        "-" +
                        entryDate.month.toString() +
                        "-" +
                        entryDate.day.toString(),
                    contactNumber: contactText.text,
                    passportPage1: url1,
                    passportPage2: url2,
                    passportPage3: url3,
                    personalPhoto: url4,
                    ticketCopy: url5));
          }else if(EmailValidator.validate(emailText.text)==false){
            snackBar('please enter a valid email');
          } else {
            snackBar('enter all fields');
          }
        },
        child: Icon(Icons.done),
      ),
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Color(0xFF016BA6),
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                    (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 5.2),
            ),
            Text(
              'Apply for Visa',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: loading == true
          ? Center(
        child: JumpingDotsProgressIndicator(
          fontSize: 70.0,
          numberOfDots: 5,
          color: Constants.kitGradients[3],
        ),
      )
          : Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Stack(
          children: [
            Positioned(
              top: screenHeight(context, dividedBy: 38.0),
              child: ApplyVisaTextField(
                controller: fullnameText,
                hint: 'Enter Full name',
                label: 'Full name',
              ),
            ),
            Positioned(
              top: screenHeight(context, dividedBy: 10.0),
              child: ApplyVisaTextField(
                controller: emailText,
                hint: 'Enter Email address',
                label: 'Email',
              ),
            ),
            Positioned(
              top: screenHeight(context, dividedBy: 6.0),
              child: ApplyVisaTextField(
                controller: countryCodeText,
                hint: 'Choose code',
                label: 'Country code',
              ),
            ),
            Positioned(
                top: screenHeight(context, dividedBy: 4.2),
                child: Container(
                  // color: Colors.red,
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 12),
                  child: TextField(
                    onTap: () {
                      showAlertDialog(context);
                    },
                    readOnly: true,
                    decoration: InputDecoration(
                      labelText: 'Date of entry',
                      labelStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins',
                          color: Constants.kitGradients[4]),
                      hintText: entryDate.day.toString() +
                          " - " +
                          entryDate.month.toString() +
                          " - " +
                          entryDate.year.toString(),
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins',
                          color: Constants.kitGradients[4]),
                    ),
                  ),
                )),
            Positioned(
              top: screenHeight(context, dividedBy: 3.3),
              child: ApplyVisaTextField(
                controller: nationalityText,
                hint: 'Choose nationality',
                label: 'Nationality',
              ),
            ),
            Positioned(
              top: screenHeight(context, dividedBy: 2.7),
              child: ApplyVisaMobileTextField(
                controller: contactText,
                hint: 'Enter Contact number',
                label: 'Contact Number',
              ),
            ),
            Positioned(
              top: screenHeight(context, dividedBy: 2.2),
              child: Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 2.3),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ImageUpload(
                            title1: "Passport Page 1",
                            title2: "tap to upload",
                            onChange: (url) {
                              setState(() {
                                url1 = url;
                              });
                            },
                          ),
                          ImageUpload(
                            title1: "Passport Page 2",
                            title2: "tap to upload",
                            onChange: (url) {
                              setState(() {
                                url2 = url;
                              });
                            },
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ImageUpload(
                            title1: "Passport Page 3",
                            title2: "tap to upload",
                            onChange: (url) {
                              setState(() {
                                url3 = url;
                              });
                            },
                          ),
                          ImageUpload(
                            title1: "Personal photo",
                            title2: "tap to upload",
                            onChange: (url) {
                              url4 = url;
                            },
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ImageUpload(
                            title1: "Ticket copy",
                            title2: "tap to upload",
                            onChange: (url) {
                              url5 = url;
                            },
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 2.3),
                            height: screenHeight(context, dividedBy: 8),
                          ),
                        ],
                      )
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("Done"),
      onPressed: () {
        if (entryDate == null) {
          entryDate = DateTime.now();
        }
        Navigator.of(context).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Center(
          child: Text(
            "Select Date",
            style: TextStyle(color: Constants.kitGradients[4]),
          )),
      content: CalendarHolo(
        onChange: (newDate) {
          setState(() {
            entryDate = newDate;
          });
          print("via" + newDate.toString());
        },
      ),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  snackBar(String status){
    final snack = SnackBar(
      content: Text(status.toString()),
      duration: Duration(seconds: 2),
    );
    _globalKey.currentState.showSnackBar(snack);
  }
}
