import 'dart:async';


import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/models/contact_us_request.dart';
import 'package:smart_travel/models/status_request.dart';
import 'package:smart_travel/models/upload_image_request.dart';
import 'package:smart_travel/utils/urls.dart';

import 'object_factory.dart';

class ApiClient {
  ApiClient() {
    initClient();
  }

//for api client testing only
  ApiClient.test({@required this.dio});

  Dio dio;


  initClient() async {
    dio = Dio();


    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions reqOptions) {
        return reqOptions;
      },
      onError: (DioError dioError) {
        return dioError.response;
      },
    ));
  }

  ///visa list
  Future<Response> visaList() {
    FormData formData = new FormData.fromMap({
      "api_key": "9fef391a-596a-4f04-81d9-e3fa4a5ebfe3",
    });
    return dio.post(Urls.visaListUrl, data: formData);
  }

  Future<Response> applyVisa(ApplyVisaRequest applyVisaRequest){

    FormData formData = new FormData.fromMap({
      "api_key": applyVisaRequest.apiKey,
      "visa" : applyVisaRequest.visa,
      "name" : applyVisaRequest.name,
      "email" : applyVisaRequest.email,
      "country_code" : applyVisaRequest.countryCode,
      "contact_number" : applyVisaRequest.contactNumber,
      "date_of_entry" : applyVisaRequest.dateOfEntry,
      "nationality" : applyVisaRequest.nationality,
      "passport_page_1" : applyVisaRequest.passportPage1,
      "passport_page_2" : applyVisaRequest.passportPage2,
      "passport_page_3" : applyVisaRequest.passportPage3,
      "personal_photo" : applyVisaRequest.personalPhoto,
      "ticket_copy" : applyVisaRequest.ticketCopy
    });
    return dio.post(Urls.applyForVisaUrl, data: formData);
  }

  Future<Response> uploadImage(UploadImageRequest uploadImageRequest)async {
    String fileName = uploadImageRequest.imageFile.path
        .split('/')
        .last;
    FormData formData = FormData.fromMap({
      "api_key": uploadImageRequest.apiKey,
      "image_file": await MultipartFile.fromFile(
          uploadImageRequest.imageFile.path, filename: fileName),
    });
    return await dio.post(Urls.uploadImageUrl, data: formData);
  }

  Future<Response> contactUs(ContactUsRequest contactUsRequest)async {
    FormData formData = FormData.fromMap({
      "api_key": contactUsRequest.apiKey,
      "name": contactUsRequest.name,
      "email": contactUsRequest.email,
      "contact_number": contactUsRequest.contactNumber,
      "subject": contactUsRequest.subject,
      "message": contactUsRequest.message
    });
    return await dio.post(Urls.contactUsUrl, data: formData);
  }


  Future<Response> statusCheck(StatusRequest statusRequest)async {
    FormData formData = FormData.fromMap({
      "api_key": statusRequest.apiKey,
      "application_id":statusRequest.applicationId
    });
    return await dio.post(Urls.statusUrl, data: formData);
  }
}
