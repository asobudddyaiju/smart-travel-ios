import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/models/apply_visa_response.dart';
import 'package:smart_travel/models/contact_us_request.dart';
import 'package:smart_travel/models/contact_us_response.dart';
import 'package:smart_travel/models/state.dart';
import 'package:smart_travel/models/status_request.dart';
import 'package:smart_travel/models/status_response.dart';
import 'package:smart_travel/models/upload_image_request.dart';
import 'package:smart_travel/models/upload_image_response.dart';
import 'package:smart_travel/models/visa_list_model.dart';
import 'package:smart_travel/utils/object_factory.dart';

class ApiProvider {
  Future<State> visaList() async {
    final response = await ObjectFactory().apiClient.visaList();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<VisaListResponse>.success(
          VisaListResponse.fromJson(response.data));
    } else {
      return State<VisaListResponse>.error(
          VisaListResponse.fromJson(response.data));
    }
  }

  Future<State> applyVisa(ApplyVisaRequest applyVisaRequest) async {
    final response = await ObjectFactory().apiClient.applyVisa(applyVisaRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<ApplyVisaResponse>.success(
          ApplyVisaResponse.fromJson(response.data));
    } else {
      return State<ApplyVisaResponse>.error(
          ApplyVisaResponse.fromJson(response.data));
    }
  }

  Future<State> uploadImage(UploadImageRequest uploadImageRequest) async {
    final response = await ObjectFactory().apiClient.uploadImage(uploadImageRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UploadImageResponse>.success(
          UploadImageResponse.fromJson(response.data));
    } else {
      return State<UploadImageResponse>.error(
          UploadImageResponse.fromJson(response.data));
    }
  }

  Future<State> contactUs(ContactUsRequest contactUsRequest) async {
    final response = await ObjectFactory().apiClient.contactUs(contactUsRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<ContactUsResponse>.success(
          ContactUsResponse.fromJson(response.data));
    } else {
      return State<ContactUsResponse>.error(
          ContactUsResponse.fromJson(response.data));
    }
  }

  Future<State> statusCheck(StatusRequest statusRequest) async {
    final response = await ObjectFactory().apiClient.statusCheck(statusRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<StatusResponse>.success(
          StatusResponse.fromJson(response.data));
    } else {
      return State<StatusResponse>.error(
          StatusResponse.fromJson(response.data));
    }
  }

}
